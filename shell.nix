let
  pkgs = import <nixpkgs> {};
in pkgs.stdenv.mkDerivation rec {
  name = "gimmie-star";
  buildInputs = [
    pkgs.cargo
    pkgs.rustc
    pkgs.rustfmt
  ];
}
