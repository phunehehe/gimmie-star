// This module explores how integer multiplication might be implemented if it were not available

use bit_vec::BitVec;
use std::iter::repeat;

// The obvious implementation is to add a number multiple times
fn multiply1(lhs: usize, rhs: usize) -> usize {
    let mut result = 0;
    for _ in 0..rhs {
        result += lhs
    }
    result
}

// Thinking at a higher level the same idea can be implemented by operating on a collection,
// resulting more readable code
fn multiply2(lhs: usize, rhs: usize) -> usize {
    repeat(lhs).take(rhs).sum()
}

// The repeated addition is a limiting factor. A cheap way to mitigate that in some cases is to
// choose the smaller number as the right hand side to reduce the number of addition rounds. This
// obviously doesn't help if both numbers are large.
fn multiply3(lhs: usize, rhs: usize) -> usize {
    if lhs > rhs {
        multiply2(lhs, rhs)
    } else {
        multiply2(rhs, lhs)
    }
}

// To support negative numbers, the signs can be processed separately
fn multiply4(lhs: isize, rhs: isize) -> isize {
    let result = multiply3(lhs.abs() as usize, rhs.abs() as usize) as isize;
    if lhs.signum() == rhs.signum() {
        result
    } else {
        -result
    }
}

// Ultimately the performance problem remains. Multiplication is not usually performed by adding
// again and again, because it's impractical. Thinking at a lower level, the technique taught in
// grade school can be adapted. The left hand side is "multiplied" with each digit on the right
// hand side, then shifted and added. This ends up very nicely in binary, because "multiplying" by
// 0 and 1 is trivial.
fn multiply5(lhs: isize, rhs: isize) -> isize {
    let mut result = 0;
    for bit in BitVec::from_bytes(&rhs.to_be_bytes()) {
        result <<= 1;
        if bit {
            result += lhs
        }
    }
    result
}

// The optimization in multiply3 can be applied to multiply5 as well, for a little bit of
// performance gain

// The negative support in multiply4 is not needed at all, due to the use of two's complement to
// represent signed numbers

// Some concluding thoughts:
// - None of the methods above handles overflow
// - Instead of usize and isize the functions can be updated to accept any type that implements
// traits such as Shl
// - Elegance and speed is sometimes a trade-off

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_multiply() {
        assert_eq!(multiply1(2, 3), 6);
        assert_eq!(multiply2(2, 3), 6);
        assert_eq!(multiply3(2, 999_999_999), 1999999998);

        assert_eq!(multiply4(-2, -3), 6);
        assert_eq!(multiply4(-2, 3), -6);
        assert_eq!(multiply4(2, -3), -6);
        assert_eq!(multiply4(2, 3), 6);

        assert_eq!(multiply5(-2, -3), 6);
        assert_eq!(multiply5(-2, 3), -6);
        assert_eq!(multiply5(2, -3), -6);
        assert_eq!(multiply5(999_999_999, 999_999_999), 999999998000000001);
    }
}
